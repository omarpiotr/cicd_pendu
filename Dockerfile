FROM python:alpine3.9

RUN mkdir /code
WORKDIR /code
ADD . /code/

EXPOSE 80
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD [ "python", "./main.py" ]