from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def connexion():
    return render_template("index.html", conexion=0)

@app.route('/coucou')
def coucou():
    return render_template("coucou.html", conexion=0)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80, debug=True)
